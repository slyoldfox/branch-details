package com.caseware.stash.rest;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.stash.nav.NavBuilder;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.atlassian.stash.scm.git.GitCommand;
import com.atlassian.stash.scm.git.GitCommandBuilderFactory;

@Path("/BranchDetails")
@AnonymousAllowed
@Produces({MediaType.APPLICATION_JSON})
public class BranchDetails {

	private final RepositoryService _repoService;
	private final GitCommandBuilderFactory _gitCommandBuilderFactory;
    private final NavBuilder _navBuilder;

	public BranchDetails(final GitCommandBuilderFactory gitCommandBuilderFactory, final RepositoryService repoService, final NavBuilder navBuilder) {
		_gitCommandBuilderFactory = gitCommandBuilderFactory;
		_repoService = repoService;
        _navBuilder = navBuilder;
	}

	@GET
	@Path("projects/{projectKey}/repos/{repoSlug}/commits/{guid}/value")
	public Response getBranchForCommit(@PathParam("projectKey") final String projectKey, @PathParam("repoSlug") final String repoSlug, @PathParam("guid") final String guid) throws JSONException {
		final Repository repo = _repoService.getBySlug(projectKey, repoSlug);

        NavBuilder.ListCommits listCommits = _navBuilder.repo(repo).commits();
		final GitCommand<String> command = _gitCommandBuilderFactory.builder(repo)
				.command("branch")
				.argument("--contains")
				.argument(guid)
				.build(new GitStringOutputHandler());

		final String result = command.call();

		final StringBuilder sbResult = new StringBuilder();
		boolean addComma = false;
		for (final String token : result.split("\n")) {
			if (addComma)
				sbResult.append(", ");
			else
				addComma = true;
            sbResult.append(token.trim());
		}

        JSONObject jsonObject = new JSONObject();
        jsonObject.put( "baseUrl", listCommits.buildAbsolute());
        jsonObject.put( "result", sbResult.toString().replace("* ", "") );

		final ResponseBuilder response = Response.status(Response.Status.OK).entity(jsonObject.toString());
		return response.build();
	}
}
